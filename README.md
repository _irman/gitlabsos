# gitlabsos

gitlabsos provides a unified method of gathering info and logs from GitLab
and the system it's running on.

## When you should run the script

If performance is degraded, then the script works best if you run it _while_
you're experiencing the issue.

If you're reproducing a software error (like a 500 error), then the script works
best if you run it just _after_ the issue.

Don't fret if you miss your window of opportunity. Run gitlabsos
anyway and it will still gather a wealth of helpful information for
identifying issues.

## Limitations

- **Only works on Omnibus installs**. This means that the script won't collect
    GitLab-related information for k8s chart deployments or source installs.
- The resulting archive _could_ end up being large. In an attempt to reduce the
    log and archive size, gitlabsos only grabs the last 10MB of each log file.

## Get started

### Run the script

The gitlabsos program is a simple ruby script, and is designed to run "out of
the box" without any further configuration. See below for an example of how
you can execute the script.

```sh
/opt/gitlab/embedded/bin/git clone https://gitlab.com/gitlab-com/support/toolbox/gitlabsos.git && cd gitlabsos
./gitlabsos.rb
```

**Execute directly**

```sh
curl https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/raw/master/gitlabsos.rb | /opt/gitlab/embedded/bin/ruby
```

### Output

The script will log some output to your terminal that should look something like this:

```text
root@gitlabhost:/home/user1/gitlabsos# ./gitlabsos.rb
[2019-08-08T19:43:19.552020] INFO -- gitlabsos: Starting gitlabsos report
[2019-08-08T19:43:19.552141] INFO -- gitlabsos: Gathering configuration and system info..
[2019-08-08T19:43:19.554574] INFO -- gitlabsos: Collecting diagnostics. This will probably take a few minutes..
[2019-08-08T19:43:19.585381] WARN -- gitlabsos: command 'getenforce' doesn't exist
[2019-08-08T19:43:19.588019] WARN -- gitlabsos: command 'sestatus' doesn't exist
[2019-08-08T19:44:12.906677] WARN -- gitlabsos: command 'iotop -aoPqt -b -d 1 -n 10' doesn't exist
[2019-08-08T19:44:33.692803] INFO -- gitlabsos: Getting GitLab logs..
[2019-08-08T19:44:33.790908] INFO -- gitlabsos: Getting unicorn worker active/queued stats..
[2019-08-08T19:44:48.811356] INFO -- gitlabsos: Report finished.
/tmp/gitlabsos.user1-gitlabomnibus_20190808194319.tar.bz2
```

You'll see several INFO and WARN level log entries. As long as you see "Report
finished" at the end, followed by the path to the tar archive, then you can
safely ignore the rest of the output.
